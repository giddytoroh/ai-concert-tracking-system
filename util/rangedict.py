class RangeDict:
    def __init__(self, demarcations, values, *, lowerbound=None, upperbound=None,
                 loweropen=False, upperopen=False, equalcase="leftclose"):
        assert len(demarcations) + 1 == len(values)
        assert (lowerbound is None or lowerbound < demarcations[0])
        assert (upperbound is None or upperbound > demarcations[-1])
        self.demarcations = sorted(demarcations)
        self.values = list(values)
        self.lowerbound = lowerbound
        self.upperbound = upperbound
        self.loweropen = loweropen
        self.upperopen = upperopen
        self.equalcase = equalcase
        if self.equalcase != "leftclose" and self.equalcase != "rightclose":
            raise ValueError

    def __getitem__(self, index):
        if isinstance(index, int) or isinstance(index, float):
            v_idx = self._get_value_index(index)
            return self.values[v_idx]
        elif isinstance(index, slice):
            if index.step is not None:
                raise ValueError
            if index.start is None:
                l_idx = 0
            else:
                l_idx = self._get_value_index(index.start)
                if self.equalcase == "rightclose" and index.start == self.demarcations[l_idx]:
                    l_idx += 1
            if index.stop is None:
                u_idx = len(self.values)
            else:
                u_idx = self._get_value_index(index.stop)
                if self.equalcase == "rightclose" or index.stop != self.demarcations[u_idx-1]:
                    if index.stop != index.start:
                        u_idx += 1
            return self.values[l_idx:u_idx]
        else:
            raise TypeError

    def _get_value_index(self, idx):
        if self.lowerbound is not None:
            if idx < self.lowerbound:
                raise KeyError
            if idx == self.lowerbound and self.loweropen:
                raise KeyError
        if self.upperbound is not None:
            if idx > self.upperbound:
                raise KeyError
            if idx == self.upperbound and self.upperopen:
                raise KeyError
        if idx < self.demarcations[0]:
            return 0
        if idx > self.demarcations[-1]:
            return len(self.values)-1

        l = 0
        u = len(self.demarcations)-1
        if idx == self.demarcations[l]:
            return l+1 if self.equalcase == "leftclose" else l
        if idx == self.demarcations[u]:
            return u+1 if self.equalcase == "leftclose" else u
        while l+1 < u:
            m = (l+u) // 2
            if idx == self.demarcations[m]:
                return m+1 if self.equalcase == "leftclose" else m
            elif idx < self.demarcations[m]:
                u = m
            else:
                l = m
        return u
    