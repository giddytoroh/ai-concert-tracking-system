import sys
import numpy as np
import soundfile
import librosa
from musictracking.util import get_chroma_feature_from_spectrogram
from musictracking.util import Expandable2DArray
from musictracking.onlinedtw import ODTW
from musictracking.musictracker import get_feature_from_frames

class AlignmentLog:
    def __init__(self):
        self.X_time = []
        self.Y_time_raw = []

    def addxy(self, x_time, y_time):
        if len(self.X_time) == 0 or x_time > self.X_time[-1]:
            self.X_time.append(x_time)
            self.Y_time_raw.append([])
        elif x_time < self.X_time[-1]:
            print(x_time, self.X_time[-1])
            raise RuntimeError
        self.Y_time_raw[-1].append(y_time)

    def average(self):
        assert len(self.X_time) == len(self.Y_time_raw)
        for i in range(len(self.X_time)):
            self.Y_time_raw[i] = [sum(self.Y_time_raw[i])/len(self.Y_time_raw[i])] \
                                 * len(self.Y_time_raw[i])
        self.Y_time = [y[0] for y in self.Y_time_raw]
        assert len(self.X_time) == len(self.Y_time)

    def count_tempo(self):
        tempo_num = 20
        temp_tempo = [1] * tempo_num
        tempo_weight = np.arange(1, 1 + tempo_num)
        tempo_weight_sum = tempo_weight.sum()
        self.tempo = []
        for i in range(len(self.X_time)):
            t = 1
            if i >= 100:
                t = (self.Y_time[i]-self.Y_time[i-100]) / (self.X_time[i] - self.X_time[i-100])
            elif i > 0:
                t = (self.Y_time[i]-self.Y_time[0]) / (self.X_time[i] - self.X_time[0])
            temp_tempo.append(t)
            now_t = (temp_tempo[-tempo_num:] * tempo_weight).sum() / tempo_weight_sum
            self.tempo.append(now_t)
        assert len(self.X_time) == len(self.tempo)

    def smooth(self):
        self.average()
        self.count_tempo()
        self.new_Y_time = [self.Y_time[0]]
        print(self.X_time[0], self.new_Y_time[0])

        for i in range(1, len(self.X_time)):
            if abs(self.new_Y_time[i-1] - self.Y_time[i-1]) > 0.4:
                self.new_Y_time.append(self.Y_time[i])
            else:
                dt = self.X_time[i] - self.X_time[i-1]
                next_y_t = self.new_Y_time[i-1] + dt * self.tempo[i-1]
                next_y_t += 2 * dt * (self.Y_time[i] - next_y_t)
                self.new_Y_time.append(next_y_t)
            print(round(self.X_time[i], 3), round(self.Y_time[i], 3),
                  round(self.new_Y_time[i], 3), round(self.tempo[i], 3))
        assert len(self.X_time) == len(self.new_Y_time)

ref_filename = sys.argv[1]
real_filename = sys.argv[2]

ref_frames, ref_rate = soundfile.read(ref_filename, always_2d=True)
real_frames, real_rate = soundfile.read(real_filename, always_2d=True)
ref_frames = ref_frames[:, 0]
real_frames = real_frames[:, 0]
if ref_rate != real_rate:
    real_frames = librosa.resample(real_frames, real_rate, ref_rate)
window_size = (2048 * ref_rate) // 44100
hop_size = (882 * ref_rate) // 44100
FEATURE_DIFF = True
ref_feature, ref_time = get_feature_from_frames(ref_frames, ref_rate, window_size, hop_size, FEATURE_DIFF)
real_feature, real_time = get_feature_from_frames(real_frames, ref_rate, window_size, hop_size, FEATURE_DIFF)

real_feature = Expandable2DArray(real_feature)
odtw = ODTW(real_feature, ref_feature, c=500, X_start=0, use_tempo_model=True, Y_time=ref_time)
while odtw.x + 1 < real_feature.shape[1]:
    odtw.update(max_tj=2)
    print(odtw.Y_time[odtw.y], real_time[odtw.x])
wp = odtw.backtrack()
rft = np.array(odtw.Y_time)[wp[:,1]][::-1]
rlt = real_time[wp[:,0]][::-1]

olog = AlignmentLog()
for i in range(len(wp)):
    olog.addxy(rlt[i], rft[i])
olog.smooth()
nt = np.ones(len(olog.X_time))
nx = np.array(olog.X_time)
ny = np.array(olog.new_Y_time)
nt[1:-1] = (ny[2:] - ny[:-2]) / (nx[2:] - nx[:-2])
nt[0] = nt[1]
nt[-1] = nt[-2]
prefix = ref_filename.split('\\')[-1].split('.')[0]
np.savez("resource\\"+prefix+"_real", real_time=olog.X_time,
         ref_time=olog.Y_time, tempo1=olog.tempo, tempo2=nt)