from .rangedict import RangeDict
import mido
import time

def note_init():
    return {"on": {}, "going": {}, "off": {}, "program":{}, "program_changed": False}

class MIDIDummyOutput:
    def send(self, message):
        pass

dummy_output = MIDIDummyOutput()

class MIDITimePlayer:
    def __init__(self, midi, output=dummy_output, velocity=1):
        self.midi_initialize(midi)
        self.output = output
        self.v = velocity
        self.reset()

    def midi_initialize(self, midi):
        program = {}
        for msg in midi:
            if hasattr(msg, "channel") and msg.type != "channel_prefix":
                program[msg.channel] = 0
        time = 0.0
        time_splits = []
        notes = [note_init()]
        playing = {}
        program_changed = False
        for msg in midi:
            if msg.time != 0:
                for note in playing:
                    if note not in notes[-1]["on"] \
                       and playing[note] > 0:
                        notes[-1]["going"][note] = playing[note]
                notes[-1]["program_changed"] = program_changed
                notes[-1]["program"] = program.copy()
                time += msg.time
                time_splits.append(time)
                notes.append(note_init())
                program_changed = False

            if msg.type == "program_change":
                program[msg.channel] = msg.program
                program_changed = True
            elif msg.type == "note_on" or msg.type == "note_off":
                if msg.type == "note_on" and msg.velocity > 0:
                    notes[-1]["on"][(msg.channel, msg.note)] = msg.velocity
                    playing[(msg.channel, msg.note)] = msg.velocity
                else:
                    notes[-1]["off"][(msg.channel, msg.note)] = 0
                    playing[(msg.channel, msg.note)] = 0

        self.notes = RangeDict(time_splits[:-1], notes[:-1])
        self.length = midi.length

    def reset(self):
        self.play_time = -1.0
        self.playing = {}
        self.last_note = note_init()

    def play(self, time):
        if time >= self.length or time < 0:
            self.stop()
            return False

        self.play_time = time
        note = self.notes[time]
        if note is not self.last_note:
            self.last_note = note
            for ch in note["program"]:
                self.output.send(mido.Message('program_change', channel=ch, program=note["program"][ch]))
            for n in note["off"]:
                self.output.send(mido.Message('note_off', channel=n[0], note=n[1]))
                self.playing[n] = 0
            for n in note["on"]:
                self.output.send(mido.Message('note_on', channel=n[0], note=n[1],
                                              velocity=(round(note["on"][n] * self.v))))
                self.playing[n] = note["on"][n]
            for n in note["going"]:
                if n not in self.playing or self.playing[n] == 0:
                    self.output.send(mido.Message('note_on', channel=n[0], note=n[1],
                                                  velocity=(round(note["going"][n] * self.v))))
                    self.playing[n] = note["going"][n]
            for n in self.playing:
                if self.playing[n] > 0 and n not in note["on"] and n not in note["going"]:
                    self.output.send(mido.Message('note_off', channel=n[0], note=n[1]))
                    self.playing[n] = 0
        return True

    def stop(self):
        for n in self.playing:
            self.output.send(mido.Message('note_off', channel=n[0], note=n[1]))
        self.reset()

MIDIPlayer = MIDITimePlayer

if __name__ == "__main__":
    import sys
    mid = mido.MidiFile(sys.argv[1])
    output = mido.open_output(mido.get_output_names()[0])

    player = MIDITimePlayer(mid, output)
    print(player.length)
    start_time = time.time()
    while True:
        t = time.time() - start_time
        if not player.play(t):
            break
        #time.sleep(0.001)