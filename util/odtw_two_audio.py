import sys
import numpy as np
import soundfile
import librosa
from musictracking.util import get_chroma_feature_from_spectrogram
from musictracking.util import Expandable2DArray
from musictracking.onlinedtw import ODTW
from musictracking.musictracker import get_feature_from_frames

ref_filename = sys.argv[1]
real_filename = sys.argv[2]

ref_frames, ref_rate = soundfile.read(ref_filename, always_2d=True)
real_frames, real_rate = soundfile.read(real_filename, always_2d=True)
ref_frames = ref_frames[:, 0]
real_frames = real_frames[:, 0]
if ref_rate != real_rate:
    real_frames = librosa.resample(real_frames, real_rate, ref_rate)
window_size = (2048 * ref_rate) // 44100
hop_size = (882 * ref_rate) // 44100
FEATURE_DIFF = True
ref_feature, ref_time = get_feature_from_frames(ref_frames, ref_rate, window_size, hop_size, FEATURE_DIFF)
real_feature, real_time = get_feature_from_frames(real_frames, ref_rate, window_size, hop_size, FEATURE_DIFF)

real_feature = Expandable2DArray(real_feature)
odtw = ODTW(real_feature, ref_feature, c=500, X_start=0, use_tempo_model=True, Y_time=ref_time)
while odtw.x + 1 < real_feature.shape[1] or odtw.ref_pos + 1 < ref_feature.shape[1]:
    odtw.update(max_tj=2)
    print(odtw.Y_time[odtw.y], real_time[odtw.x])
wp = odtw.backtrack()
rft = np.array(odtw.Y_time)[wp[:,1]][::-1]
rlt = real_time[wp[:,0]][::-1]
#time_list = np.stack([rlt, rft], axis=-1)
#for t in time_list:
#    print(t)
#for i in range(len(rlt)):
#    print(rft[i], rlt[i])

def hanning_smooth(x, r):
    hann_filter = np.hanning(2 * r + 1)
    hann_filter = hann_filter / hann_filter.sum()
    x_ = np.convolve(np.pad(x, (r, r), mode="edge"), hann_filter, mode="valid")
    if len(x) != len(x_):
        raise RuntimeError
    return x_

nrft = hanning_smooth(rft, 10)
nrlt = hanning_smooth(rlt, 10)
tempo1 = np.ones(len(rlt))
for i in range(len(rlt)):
    ii, j = i, i
    while nrft[j] - nrft[ii] < 0.2 and j < len(rlt) - 1:
        j += 1
    while nrft[j] - nrft[ii] < 0.2 and ii > 0:
        ii -= 1
    tempo1[i] = (nrlt[j] - nrlt[ii]) / (nrft[j] - nrft[ii])
    print(rft[i], rlt[i], tempo1[i], ii, j)

prefix = ref_filename.split('\\')[-1].split('.')[0]
np.savez("resource\\"+prefix+"_real", real_time=rlt, ref_time=rft, tempo1=tempo1)