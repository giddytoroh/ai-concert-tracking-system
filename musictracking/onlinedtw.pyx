# cython: language_level=3

import numpy as np
cimport numpy as np
from random import random as random_0_1
from scipy.spatial.distance import cdist
from .util import Expandable2DArray

cdef class ExpandableLILMatrix:
    cdef np.npy_intp _shape_j
    cdef default_val
    cdef list data, row_start

    def __init__(self, *, np.ndarray arr=None,
                 (int, int) shape=(0,0), default_val=0):
        self.default_val = default_val
        if arr is not None:
            arr = np.atleast_2d(arr)
            if arr.ndim > 2:
                raise ValueError
            self._shape_j = arr.shape[1]
            self.data = arr.tolist()
            self.row_start = [0] * len(self.data)
        else:
            self.data = [None] * shape[0]
            self.row_start = [None] * shape[0]
            self._shape_j = shape[1]

    cpdef void add_rows(self, unsigned int r):
        self.data.extend([None] * r)
        self.row_start.extend([None] * r)

    cpdef void add_cols(self, unsigned int c):
        self._shape_j += c

    cpdef void reserve_row(self, int i, int start, int size):
        if self.row_start[i] == None:
            self.row_start[i] = start
            self.data[i] = [self.default_val] * size

    cdef np.ndarray _parse_slice(self, slice sl, np.npy_intp shape):
        start = sl.start
        stop = sl.stop
        step = sl.step
        if step is not None and step < 0:
            if start == None:
                start = shape - 1
            if stop == None:
                stop = -1
        else:
            if start == None:
                start = 0
            if stop == None:
                stop = shape
        return np.arange(start, stop, step)

    cdef _get(self, int i, int j):
        if i < 0 or i >= len(self.data):
            raise IndexError("Index out of bound. ({})".format(i))
        if j < 0 or j >= self._shape_j:
            raise IndexError("Index out of bound. ({})".format(j))
        return self._get_nocheck(i, j)

    cdef _get_nocheck(self, int i, int j):
        cdef int rsi
        if self.row_start[i] is not None:
            rsi = self.row_start[i]
            if j >= rsi and j < rsi + len(self.data[i]):
                return self.data[i][j-rsi]
        return self.default_val

    cdef np.ndarray _get2(self, np.ndarray ii, np.ndarray jj):
        if ii.min() < 0 or ii.max() >= len(self.data):
            raise IndexError("Index out of bound. ({}/{})".format(ii.min(), ii.max()))
        if jj.min() < 0 or jj.max() >= self._shape_j:
            raise IndexError("Index out of bound. ({}/{})".format(jj.min(), jj.max()))

        cdef np.ndarray i_index, j_index
        i_index, j_index = np.broadcast_arrays(ii, jj)

        if i_index.size == 0:
            return np.full((i_index.shape[0], i_index.shape[1]), self.default_val, dtype=np.float64)

        if i_index.ndim == 1:
            n_shape = (i_index.shape[0],)
        else:
            n_shape = (i_index.shape[0], i_index.shape[1])
        arr = np.full(n_shape, self.default_val, dtype=np.float64)

        cdef int idx1, idx2
        if i_index.ndim == 1:
            for idx1 in range(i_index.shape[0]):
                arr[idx1] = self._get_nocheck(i_index[idx1], j_index[idx1])
        else:
            for idx1 in range(i_index.shape[0]):
                for idx2 in range(i_index.shape[1]):
                    arr[idx1, idx2] = self._get_nocheck(i_index[idx1, idx2], j_index[idx1, idx2])

        return arr

    def __getitem__(self, index):
        return self._getitem(index)

    cdef _getitem(self, index):
        cdef np.ndarray i_index, j_index
        if isinstance(index, tuple):
            if len(index) == 2:
                i_isint = isinstance(index[0], int)
                j_isint = isinstance(index[1], int)
                if i_isint and j_isint:
                    return self._get(index[0], index[1])

                i_isslice = isinstance(index[0], slice)
                j_isslice = isinstance(index[1], slice)
                if i_isslice:
                    i_index = self._parse_slice(index[0], len(self.data))
                else:
                    i_index = np.atleast_1d(index[0])
                    if i_index.ndim > 1 or i_index.dtype.kind == 'b':
                        raise NotImplementedError
                if j_isslice:
                    j_index = self._parse_slice(index[1], self._shape_j)
                    if i_isslice:
                        i_index = i_index[:, None]
                        j_index = j_index[None, :]
                else:
                    j_index = np.atleast_1d(index[1])
                    if j_index.ndim > 1 or j_index.dtype.kind == 'b':
                        raise NotImplementedError
            else:
                raise IndexError
        else:
            if isinstance(index, slice):
                i_index = self._parse_slice(index, len(self.data))
                j_index = np.arange(self._shape_j)
                i_index = i_index[:, None]
                j_index = j_index[None, :]
            else:
                i_index = np.atleast_1d(index)
                if i_index.ndim > 1 or i_index.dtype.kind == 'b':
                    raise NotImplementedError
                j_index = np.arange(self._shape_j)

        return self._get2(i_index, j_index)

    cdef void _set(self, int i, int j, x):
        if i < 0 or i >= len(self.data):
            raise IndexError("Index out of bound. ({})".format(i))
        if j < 0 or j >= self._shape_j:
            raise IndexError("Index out of bound. ({})".format(j))

        self._set_nocheck(i, j, x)

    cdef void _set_nocheck(self, int i, int j, x):
        if self.row_start[i] is None:
            self.row_start[i] = j
            self.data[i] = [x]
            return

        cdef int rsi = self.row_start[i]
        if j < rsi:
            new_row = [self.default_val] * (rsi-j)
            new_row.extend(self.data[i])
            self.data[i] = new_row
            self.row_start[i] = j
        elif j >= rsi + len(self.data[i]):
            self.data[i].extend( [self.default_val] * (j+1-rsi-len(self.data[i])) )
        self.data[i][j-rsi] = x
        return

    cdef void _set2(self, np.ndarray ii, np.ndarray jj, np.ndarray x):
        if ii.min() < 0 or ii.max() >= len(self.data):
            raise IndexError("Index out of bound. ({}/{})".format(ii.min(), ii.max()))
        if jj.min() < 0 or jj.max() >= self._shape_j:
            raise IndexError("Index out of bound. ({}/{})".format(jj.min(), jj.max()))

        cdef np.ndarray i_index, j_index
        i_index, j_index = np.broadcast_arrays(ii, jj)

        x = np.asarray(x, dtype=np.float64)
        x, _ = np.broadcast_arrays(x, i_index)
        if i_index.ndim == 1:
            if x.shape[0] != i_index.shape[0]:
                raise ValueError("Shape mismatch in assignment.")
        if i_index.ndim == 2:
            if x.shape[0] != i_index.shape[0] or x.shape[1] != i_index.shape[1]:
                raise ValueError("Shape mismatch in assignment.")
        if np.size(x) == 0:
            return

        cdef int idx1, idx2
        if i_index.ndim == 1:
            for idx1 in range(i_index.shape[0]):
                self._set_nocheck(i_index[idx1], j_index[idx1], x[idx1])
        else:
            for idx1 in range(i_index.shape[0]):
                for idx2 in range(i_index.shape[1]):
                    self._set_nocheck(i_index[idx1, idx2], j_index[idx1, idx2], x[idx1, idx2])

    def __setitem__(self, index, x):
        self._setitem(index, x)

    cdef void _setitem(self, index, x):
        cdef np.ndarray i_index, j_index
        if isinstance(index, tuple):
            if len(index) == 2:
                i_isint = isinstance(index[0], int)
                j_isint = isinstance(index[1], int)
                if i_isint and j_isint:
                    self._set(index[0], index[1], x)
                    return

                i_isslice = isinstance(index[0], slice)
                j_isslice = isinstance(index[1], slice)
                if i_isslice:
                    i_index = self._parse_slice(index[0], len(self.data))
                else:
                    i_index = np.atleast_1d(index[0])
                    if i_index.ndim > 1 or i_index.dtype.kind == 'b':
                        raise NotImplementedError
                if j_isslice:
                    j_index = self._parse_slice(index[1], self._shape_j)
                    if i_isslice:
                        i_index = i_index[:, None]
                        j_index = j_index[None, :]
                else:
                    j_index = np.atleast_1d(index[1])
                    if j_index.ndim > 1 or j_index.dtype.kind == 'b':
                        raise NotImplementedError
            else:
                raise IndexError
        else:
            if isinstance(index, slice):
                i_index = self._parse_slice(index, len(self.data))
                j_index = np.arange(self._shape_j)
                i_index = i_index[:, None]
                j_index = j_index[None, :]
            else:
                i_index = np.atleast_1d(index)
                if i_index.ndim > 1 or i_index.dtype.kind == 'b':
                    raise NotImplementedError
                j_index = np.arange(self._shape_j)

        self._set2(i_index, j_index, x)

    @property
    def shape(self):
        cdef np.npy_intp _shape_i = len(self.data)
        return (_shape_i, self._shape_j)

ExpandableSparseMatrix = ExpandableLILMatrix

cdef class ODTW_SparseMatrix:
    cdef int MAX_RUN_COUNT
    cdef np.ndarray STEPS
    cdef np.ndarray STEP_WEIGHT
    # STEP_CODE: >= 0 for each step in STEPS,
    #            -1 for start, -2 for undetermined
    cdef readonly object X, Y
    cdef readonly ExpandableLILMatrix C, D, pathL, pathDir
    cdef np.ndarray Y_ref, Y_ref_time
    cdef readonly list Y_time
    cdef str metric
    cdef int prev_expand, prev_ref
    cdef list eval_start
    cdef readonly int t, j, x, y, c, X_start, ref_pos
    cdef int run_count, ref_count
    cdef list tempo_list
    cdef np.ndarray tempo_weight
    cdef int use_tempo_model, tempo_number, tempo_window, tempo_weight_sum, tempo_count_offset
    cdef readonly double X_time_diff, now_tempo

    def __init__(self, object X, np.ndarray Y, int c, int X_start=0,
                 np.ndarray Y_time=None, use_tempo_model=True, str metric="euclidean"):
        self.MAX_RUN_COUNT = 3
        self.STEPS = np.array([[1, 1], [0, 1], [1, 0]])
        self.STEP_WEIGHT = np.array([1.0, 1.0, 1.0])
 
        self.tempo_window = 100
        self.tempo_count_offset = 5
        self.tempo_number = 15
        self.tempo_weight = np.arange(1, 1 + self.tempo_number)
        self.tempo_weight_sum = self.tempo_weight.sum()
        self.use_tempo_model = 1 if use_tempo_model else 0
        self.now_tempo = 1
        self.tempo_list = [self.now_tempo] * (self.tempo_number + self.tempo_count_offset)
 
        self.initialize(X, Y, c, X_start=X_start, Y_time=Y_time, metric=metric)

    cdef void initialize(self, object X, np.ndarray Y, int c, int X_start=0,
                         np.ndarray Y_time=None, str metric="euclidean"):
        if isinstance(X, np.ndarray) or isinstance(X, list):
            X = np.asarray(X)
            if X.ndim == 1:
                X = X.reshape(1,-1)
            elif X.ndim > 2:
                raise ValueError
            self.X = Expandable2DArray(X)
        else:
            if isinstance(X, Expandable2DArray):
                self.X = X
            else:
                raise ValueError
        self.Y_ref = Y
        if self.Y_ref.ndim == 1:
            self.Y_ref = self.Y_ref.reshape(1,-1)
        elif self.Y_ref.ndim > 2:
            raise ValueError
        self.Y = Expandable2DArray(self.Y_ref[:, 0].reshape(-1,1))
        if self.X.shape[0] != self.Y.shape[0]:
            raise ValueError
        self.X_start = X_start
        if Y_time is not None:
            self.Y_ref_time = Y_time
        else:
            self.Y_ref_time = np.arange(self.Y_ref.shape[1])
        self.Y_time = [self.Y_ref_time[0]]
        self.X_time_diff = self.Y_ref_time[1] - self.Y_ref_time[0]
        self.c = c
        self.metric = metric

        self.t = 0
        self.j = 0
        self.ref_pos = 0
        self.x = 0
        self.y = 0
        self.run_count = 0
        self.ref_count = 0
        self.prev_expand = -1
        self.prev_ref = 1
        C_init = cdist(self.X[:, self.X_start].reshape(1,-1), self.Y[:, 0].reshape(1,-1), metric=self.metric)
        self.C = ExpandableLILMatrix(arr=C_init, default_val=np.inf)
        self.D = ExpandableLILMatrix(arr=C_init, default_val=np.inf)
        self.pathL = ExpandableLILMatrix(arr=np.ones((1,1)), default_val=1)
        self.pathDir = ExpandableLILMatrix(arr=np.full((1,1), -1), default_val=-2) # store last step
        self.eval_start = [[0],[0]]

    cdef void _add_row(self):
        self.t += 1
        cdef int s = max(0, self.j-self.c)
        self.eval_start[0].append(s)
        if self.C.shape[0] == self.t:
            self.C.add_rows(1)
            #self.C.reserve_row(self.t, s, self.c+1)
            self.C[self.t, s:self.C.shape[1]] = cdist(self.X[:, self.X_start+self.t].reshape(1,-1),
                                                      self.Y[:, s:self.C.shape[1]].T,
                                                      metric=self.metric).reshape(-1)
        elif self.C.shape[0] < self.t:
            raise ValueError
        self.D.add_rows(1)
        self.pathL.add_rows(1)
        self.pathDir.add_rows(1)
        #self.D.reserve_row(self.t, s, self.c+1)
        #self.pathL.reserve_row(self.t, s, self.c+1)
        #self.pathDir.reserve_row(self.t, s, self.c+1)

        cdef int k, p0, p1, i, l0, l1
        cdef double new_cost
        cdef Py_ssize_t lenSTEPS = len(self.STEPS)
        for k in range(s, self.j+1):
            p0 = self.t
            p1 = k
            self.pathL[p0, p1] = p0 + p1 + 1
            for i in range(lenSTEPS):
                l0 = p0 - self.STEPS[i, 0]
                l1 = p1 - self.STEPS[i, 1]
                if l0 >= 0 and l1 >= 0:
                    new_cost = self.D[l0, l1] + self.C[p0, p1] * self.STEP_WEIGHT[i]
                    if new_cost < self.D[p0, p1]:
                        self.D[p0, p1] = new_cost
                        self.pathDir[p0, p1] = i

    cdef int _next_ref(self):
        if self.use_tempo_model == 0:
            return 1
        cdef double th
        self.now_tempo = (self.tempo_list[-self.tempo_number:] * \
                          self.tempo_weight).sum() / self.tempo_weight_sum
        th = self.now_tempo if self.now_tempo < 1 else 1 / self.now_tempo
        #print("                                                        cal tempo:", self.now_tempo, th)

        cdef double r_num = random_0_1()
        cdef int next_ref = 1
        if r_num > th:
            if self.now_tempo > 1 and self.ref_pos + 2 < self.Y_ref.shape[1]:
                next_ref = 2 # make ref faster
            elif self.now_tempo < 1:
                next_ref = 0 # make ref slower

        if next_ref == self.prev_ref:
            self.ref_count += 1
            if self.ref_count == 3:
                next_ref = 1
                self.ref_count = 1
                self.prev_ref = next_ref
        else:
            self.ref_count = 1
            self.prev_ref = next_ref

        return next_ref

    cdef void _add_Y(self, np.ndarray f, double t):
        self.Y.addcols(f)
        self.Y_time.append(t)

    cdef void _add_col(self):
        self.j += 1
        cdef int s, next_ref
        s = max(0, self.t-self.c)
        self.eval_start[1].append(s)
        if self.C.shape[1] == self.j:
            next_ref = self._next_ref()
            if next_ref == 1:
                self.ref_pos += 1
                self._add_Y(self.Y_ref[:, self.ref_pos], self.Y_ref_time[self.ref_pos])
            elif next_ref == 2: # make ref faster
                self.ref_pos += 2
                self._add_Y((self.Y_ref[:, self.ref_pos-1] + self.Y_ref[:, self.ref_pos]) / 2,
                            (self.Y_ref_time[self.ref_pos-1] + self.Y_ref_time[self.ref_pos]) / 2)
            elif next_ref == 0: # make ref slower
                self._add_Y((self.Y_ref[:, self.ref_pos] + self.Y_ref[:, self.ref_pos+1]) / 2,
                            (self.Y_ref_time[self.ref_pos] + self.Y_ref_time[self.ref_pos+1]) / 2)

            self.C.add_cols(1)
            self.C[s:self.C.shape[0], self.j] = cdist(self.X[:, self.X_start+s:self.X_start+self.C.shape[0]].T,
                                                      self.Y[:, self.j].reshape(1,-1),
                                                      metric=self.metric).reshape(-1)
        elif self.C.shape[1] < self.j:
            raise ValueError
        self.D.add_cols(1)
        self.pathL.add_cols(1)
        self.pathDir.add_cols(1)

        cdef int k, p0, p1, i, l0, l1
        cdef double new_cost
        cdef Py_ssize_t lenSTEPS = len(self.STEPS)
        for k in range(s, self.t+1):
            p0 = k
            p1 = self.j
            self.pathL[p0, p1] = p0 + p1 + 1
            for i in range(lenSTEPS):
                l0 = p0 - self.STEPS[i, 0]
                l1 = p1 - self.STEPS[i, 1]
                if l0 >= 0 and l1 >= 0:
                    new_cost = self.D[l0, l1] + self.C[p0, p1] * self.STEP_WEIGHT[i]
                    if new_cost < self.D[p0, p1]:
                        self.D[p0, p1] = new_cost
                        self.pathDir[p0, p1] = i

    cpdef _update_tj(self):
        cdef int to_expand = 0
        if self.ref_pos + 1 == self.Y_ref.shape[1]:
            to_expand = 1
        elif self.t > self.c / 2:
            if self.run_count >= self.MAX_RUN_COUNT:
                if self.prev_expand == 1:
                    to_expand = 2
                else:
                    to_expand = 1
            else:
                to_expand = self.getExpandDir(self.t, self.j)

        flag_add = True
        if to_expand != 2: # t + 1
            if self.X_start + self.t + 1 == self.X.shape[1]:
                flag_add = False
            else:
                self._add_row()
        if to_expand != 1 and flag_add: # j + 1
            if self.ref_pos + 1 == self.Y_ref.shape[1]:
                flag_add = False
            else:
                self._add_col()

        if flag_add:
            if to_expand == self.prev_expand:
                self.run_count += 1
            else:
                self.run_count = 1
            if to_expand == 0:
                self.prev_expand = -1
            else:
                self.prev_expand = to_expand

        return flag_add

    cdef int getExpandDir(self, int t, int j):
        cdef int t_s, j_s, t_min, j_min
        t_s = self.eval_start[1][j]
        t_min = (self.D[t_s:t+1, j]/self.pathL[t_s:t+1, j]).argmin() + t_s
        j_s = self.eval_start[0][t]
        j_min = (self.D[t, j_s:j+1]/self.pathL[t, j_s:j+1]).argmin() + j_s

        # Both: 0, Row: 1, Column: 2
        if self.D[t_min, j] < self.D[t, j_min]:
            return 2
        elif self.D[t_min, j] > self.D[t, j_min]:
            return 1
        else:
            return 0

    cdef void _count_tempo(self):
        cdef int xx, yy
        if self.x > self.tempo_window and self.x % self.tempo_count_offset == 0:
            xx, yy = self.backward_x(self.tempo_window)
            self.tempo_list.append((self.Y_time[self.y]-self.Y_time[yy])/((self.x-xx) * self.X_time_diff))
            #print("                                                      count tempo:", self.tempo_list[-1])
        else:
            return

    cdef _update_xy(self):
        cdef int next_step
        if self.x == self.t:
            return False
        elif self.y == self.j:
            if self.ref_pos + 1 == self.Y_ref.shape[1]:
                next_step = 1
            else:
                return False
        elif self.y < self.eval_start[0][self.x+1]:
            next_step = 2
        elif self.x < self.eval_start[1][self.y+1]:
            next_step = 1
        else:
            next_step = self.getExpandDir(self.x, self.y)

        if next_step != 2: # x + 1
            self.x += 1
            self._count_tempo()
        if next_step != 1: # y + 1
            self.y += 1

        return True

    cpdef update(self, int max_tj=1000, int max_xy=1000):
        cdef int i = 0
        cdef int k = 0
        cdef int q = 0
        while self._update_tj() and k < max_tj:
            k += 1
        while self._update_xy() and i < max_xy:
            i += 1
        while self.x < self.t and self.ref_pos + 1 < self.Y_ref.shape[1] and k < max_tj and i < max_xy:
            if q == self.MAX_RUN_COUNT:
                self.x += 1
                i += 1
                q = 0
            else:
                old_x = self.x
                self._add_col()
                k += 1
                self._update_xy()
                i += 1
                q = q + 1 if self.x == old_x else 0
        if i == 0:
            return False
        else:
            return True

    cpdef (int, int) backward(self, int max_iter):
        cdef int xx, yy, nx, ny, c
        xx = self.x
        yy = self.y
        c = 0
        while c < max_iter and self.pathDir[xx, yy] >= 0:
            nx = xx - self.STEPS[self.pathDir[xx, yy], 0]
            ny = yy - self.STEPS[self.pathDir[xx, yy], 1]
            xx = nx
            yy = ny
            c += 1

        return xx, yy

    cpdef (int, int) backward_x(self, int max_x, int max_iter=1000):
        cdef int xx, yy, nx, ny, c
        xx = self.x
        yy = self.y
        c = 0
        while self.x - xx < max_x and self.pathDir[xx, yy] >= 0 and c < max_iter:
            nx = xx - self.STEPS[self.pathDir[xx, yy], 0]
            ny = yy - self.STEPS[self.pathDir[xx, yy], 1]
            xx = nx
            yy = ny
            c += 1

        return xx, yy

    cpdef backtrack(self):
        cdef np.ndarray wp = np.full((self.x+self.x+2, 2), -1)
        cdef int wp_len = 0

        cdef int wp_t = self.x
        cdef int wp_j = self.y
        cdef n_wp_t, n_wp_j
        while self.pathDir[wp_t, wp_j] != -1:
            if wp_t < 0 or wp_j < 0 or self.pathDir[wp_t, wp_j] < -1:
                raise ValueError
            wp[wp_len, 0] = wp_t
            wp[wp_len, 1] = wp_j
            wp_len += 1
            n_wp_t = wp_t - self.STEPS[self.pathDir[wp_t, wp_j], 0]
            n_wp_j = wp_j - self.STEPS[self.pathDir[wp_t, wp_j], 1]
            wp_t = n_wp_t
            wp_j = n_wp_j
        wp[wp_len, 0] = wp_t
        wp[wp_len, 1] = wp_j
        wp_len += 1

        return wp[:wp_len]

ODTW = ODTW_SparseMatrix