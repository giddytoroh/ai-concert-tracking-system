"""
Command: python setup_odtw.py build_ext --inplace
"""

from distutils.core import setup
from Cython.Build import cythonize
import numpy

setup(ext_modules=cythonize("onlinedtw.pyx"), include_dirs=[numpy.get_include()])