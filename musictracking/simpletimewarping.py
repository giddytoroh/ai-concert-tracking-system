import numba
import numpy as np
from scipy.spatial.distance import cdist
from numba import njit as nb_njit

DEFAULT_STEPS = np.array([[1, 1], [0, 1], [1, 0]])

@nb_njit("void(f8[:,:], i4[:,:], f8[:,:], i4[:,:], f8[:], b1)")
def simpleDTW_C(D, pathDir, C, STEPS, STEP_WEIGHT, subseq):
    D[0, 0] = C[0, 0]
    pathDir[0, 0] = -1
    if subseq:
        D[0, :] = C[0, :]
        pathDir[0, :] = -1

    for t in range(C.shape[0]):
        for j in range(C.shape[1]):
            for i in range(len(STEPS)):
                last_t = t - STEPS[i, 0]
                last_j = j - STEPS[i, 1]
                if last_t >= 0 and last_j >= 0:
                    new_cost = D[last_t, last_j] + C[t, j] * STEP_WEIGHT[i]
                    if new_cost < D[t, j]:
                        D[t, j] = new_cost
                        pathDir[t, j] = i

@nb_njit("i4[:,:](i4[:,:], i4[:,:])")
def _dtw_backtrack(pathDir, STEPS):
    wp = np.full((pathDir.shape[0]+pathDir.shape[1], 2), -1, dtype=np.int32)
    wp_len = 0

    wp_t = pathDir.shape[0] - 1
    wp_j = pathDir.shape[1] - 1
    while pathDir[wp_t, wp_j] != -1:
        if wp_t < 0 or wp_j < 0 or pathDir[wp_t, wp_j] < -1:
            raise ValueError
        wp[wp_len, 0] = wp_t
        wp[wp_len, 1] = wp_j
        wp_len += 1
        n_wp_t = wp_t - STEPS[pathDir[wp_t, wp_j], 0]
        n_wp_j = wp_j - STEPS[pathDir[wp_t, wp_j], 1]
        wp_t = n_wp_t
        wp_j = n_wp_j
    wp[wp_len, 0] = wp_t
    wp[wp_len, 1] = wp_j
    wp_len += 1

    return wp[:wp_len]

def simpleDTW(X=None, Y=None, C=None, metric="euclidean", subseq=False,
              backtrack=True, STEPS=DEFAULT_STEPS, STEP_WEIGHT=None):
    if C is None and (X is None or Y is None):
        raise ValueError
    if C is not None and (X is not None or Y is not None):
        raise ValueError
    if C is None:
        X = np.atleast_2d(X)
        Y = np.atleast_2d(Y)
        C = cdist(X.T, Y.T, metric=metric)
    if STEP_WEIGHT is None:
        STEP_WEIGHT = np.ones(STEPS.shape[0])

    D = np.full(C.shape, np.inf)
    pathDir = np.full(C.shape, -2)
    simpleDTW_C(D, pathDir, C, STEPS, STEP_WEIGHT, subseq)

    if backtrack:
        if subseq:
            wp_end = np.argmin(D[-1, :])
            wp = _dtw_backtrack(pathDir[:, :wp_end+1], STEPS)
        else:
            wp = _dtw_backtrack(pathDir, STEPS)
        return D, wp
    else:
        return D

@nb_njit("void(f8[:,:], i4[:,:], i4[:,:], f8[:,:], i4[:,:], f8[:])")
def simpleGreedyBackwardTW_C(D, pathDir, pathDepth, C, STEPS, STEP_WEIGHT):
    D[0, :] = C[0, :]
    D[:, 0] = C[:, 0]
    pathDir[0, :] = -1
    pathDir[:, 0] = -1

    for t in range(1, C.shape[0]):
        for j in range(1, C.shape[1]):
            next_cost = np.inf
            for i in range(len(STEPS)):
                last_t = t - STEPS[i, 0]
                last_j = j - STEPS[i, 1]
                if last_t >= 0 and last_j >= 0:
                    new_cost = C[last_t, last_j] * STEP_WEIGHT[i]
                    if new_cost < next_cost:
                        pathDir[t, j] = i
                        pathDepth[t, j] = pathDepth[last_t, last_j] + STEPS[i, 0]
                        D[t, j] = D[last_t, last_j] - C[last_t, last_j] + new_cost + C[t, j]
                        next_cost = new_cost
                    elif new_cost == next_cost:
                        new_d = D[last_t, last_j] - C[last_t, last_j] + new_cost + C[t, j]
                        if new_d < D[t, j]:
                            pathDir[t, j] = i
                            pathDepth[t, j] = pathDepth[last_t, last_j] + STEPS[i, 0]
                            D[t, j] = new_d

def simpleGreedyBackwardTW(X=None, Y=None, C=None, metric="euclidean",
                           backtrack=True, STEPS=DEFAULT_STEPS, STEP_WEIGHT=None):
    if C is None and (X is None or Y is None):
        raise ValueError
    if C is not None and (X is not None or Y is not None):
        raise ValueError
    if C is None:
        X = np.atleast_2d(X)
        Y = np.atleast_2d(Y)
        C = cdist(X.T, Y.T, metric=metric)
    if STEP_WEIGHT is None:
        STEP_WEIGHT = np.ones(STEPS.shape[0])

    D = np.full(C.shape, np.inf)
    pathDir = np.full(C.shape, -2)
    pathDepth = np.full(C.shape, 1)
    simpleGreedyBackwardTW_C(D, pathDir, pathDepth, C, STEPS, STEP_WEIGHT)

    if backtrack:
        wp_end = np.argmin(D[-1, :]/pathDepth[-1, :])
        wp = _dtw_backtrack(pathDir[:, :wp_end+1], STEPS)
        return D/pathDepth, wp
    else:
        return D/pathDepth

@nb_njit("void(f8[:,:,:], i4[:,:,:], i4[:], f8[:,:], i4[:,:], f8[:], i4)")
def subseqDTW_window_C(D, pathDir, begin_index, C, STEPS, STEP_WEIGHT, window_size):
    for i in range(begin_index.size):
        CC = C[:, begin_index[i]:begin_index[i]+window_size]
        simpleDTW_C(D[i], pathDir[i], CC, STEPS, STEP_WEIGHT, False)

def subseqDTW_window(X=None, Y=None, C=None, metric="euclidean", window_size=None,
                     hop_size=None, backtrack=False, STEPS=DEFAULT_STEPS, STEP_WEIGHT=None):
    if C is None and (X is None or Y is None):
        raise ValueError
    if C is not None and (X is not None or Y is not None):
        raise ValueError
    if C is None:
        X = np.atleast_2d(X)
        Y = np.atleast_2d(Y)
        C = cdist(X.T, Y.T, metric=metric)
    if STEP_WEIGHT is None:
        STEP_WEIGHT = np.ones(STEPS.shape[0])
    if window_size is None:
        window_size = C.shape[0]
    if hop_size is None:
        hop_size = window_size // 2

    begin_index = np.arange(0, C.shape[1]-window_size+1, hop_size)
    end_index = begin_index + window_size - 1
    D = np.full((begin_index.size, C.shape[0], window_size), np.inf)
    pathDir = np.full((begin_index.size, C.shape[0], window_size), -2)
    subseqDTW_window_C(D, pathDir, begin_index, C, STEPS, STEP_WEIGHT, window_size)

    if backtrack:
        wp_end = D[:, -1, -1].argmin()
        wp = _dtw_backtrack(pathDir[wp_end], STEPS)
        return D, end_index, wp_end, wp
    else:
        return D, end_index

def testTW():
    import time
    import librosa
    a = np.array([1, 2, 3, 4, 5, 5, 5, 4, 3, 4, 5, 5, 5, 4])
    b = np.array([3, 4, 5, 5, 5, 4, 1, 2, 3, 4, 5, 5, 5, 4])
    C = cdist(a[:, np.newaxis], b[:, np.newaxis])
    print(C)
    for i in range(10):
        t0 = time.time()
        D, wp = simpleGreedyBackwardTW(a,b)
        print(time.time()-t0)
    print(D)
    print(wp.T)

if __name__ == "__main__":
    testTW()