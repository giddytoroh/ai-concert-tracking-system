import time
import logging
import librosa
import soundfile
import numpy as np
import multiprocessing as mp
from enum import Enum, IntEnum
from scipy.signal import spectrogram
from scipy.spatial.distance import cdist

from .recorder import RecordStream
from .onlinedtw import ODTW
from .util import convolve1d, get_chroma_feature_from_spectrogram
from .util import roll_data_to_array, roll_2Ddata_to_array_by_row
from .util import Expandable2DArray
from .simpletimewarping import simpleDTW, simpleGreedyBackwardTW

logging.basicConfig(level=logging.DEBUG)

class RunStatus(IntEnum):
    terminate = -1
    idling = 0
    running = 1
    about_to_idle = 2
    about_to_run = 3
    paused = 4
    about_to_paused = 5
    release_from_paused = 6

class MusicStatus(Enum):
    stopped = -1
    not_detected = 0
    detected = 1
    paused = 2

class ArrayOfMPArray:
    """
    This class is also applicable between processes.
    """

    def __init__(self, n, length_or_list, dtype='d'):
        if isinstance(length_or_list, int):
            ls = [length_or_list] * n
        elif isinstance(length_or_list, list):
            ls = length_or_list
        else:
            raise TypeError("length_or_list should be an int or a list.")
        self.arrays = [mp.Array(dtype, ls[i]) for i in range(n)]
        self.ids = []
        for i in range(n):
            self.ids.extend([(i, j) for j in range(ls[i])])

    def __getitem__(self, index):
        if isinstance(index, int):
            index = self.ids[index]
        return self.arrays[index[0]][index[1]]

    def __setitem__(self, index, v):
        if isinstance(index, int):
            index = self.ids[index]
        self.arrays[index[0]][index[1]] = v

    @property
    def size(self):
        return len(self.ids)

class MusicTracker:
    UPDATE_FREQ = 50
    RPE_BUFF_TIME = 9
    RPE_FEATURE_DIFF = True
    RPE_DOWN_SAMPLE = 30
    RPE_REF_RESOLUTION = 3
    RPE_RECORD_RES_RATIO = 5
    ODTW_BUFF_TIME = 2
    ODTW_FEATURE_DIFF = True
    MD_BUFF_TIME = 2
    MD_FEATURE_DIFF = True

    def __init__(self, *, ODTW_CORES, ODTWS_PER_CORE, ref_filename,
                 update_freq=None):
        self.ODTW_CORES = ODTW_CORES
        self.ODTWS_PER_CORE = ODTWS_PER_CORE
        if update_freq is not None:
            self.UPDATE_FREQ = update_freq

        self.__load_data_and_init_processes(ref_filename)

    def __load_data_and_init_processes(self, filename):
        logging.info("Loading reference audio: {}".format(filename))
        ref_frames, ref_rate = soundfile.read(filename, always_2d=True)
        ref_frames_mparr = mp.Array('d', len(ref_frames))
        ref_memory = np.frombuffer(ref_frames_mparr.get_obj())
        ref_memory[:] = ref_frames[:,0]
        self.ref_length = ref_frames.shape[0] / ref_rate
        logging.info("Reference audio loaded.")

        self._est_time = mp.Value('d', -1.0)
        self._est_tempo = mp.Value('d', -1.0)
        self.command_queue = mp.Queue()
        self.msg_queue = mp.Queue()
        self.detect_status = mp.Value('i', RunStatus.about_to_idle)
        self.rpe_status = mp.Value('i', RunStatus.about_to_idle)
        self.odtw_cores_status = [mp.Value('i', RunStatus.about_to_idle) for i in range(self.ODTW_CORES)]
        self.odtws_status = ArrayOfMPArray(self.ODTW_CORES, self.ODTWS_PER_CORE, 'i')
        self.main_status = mp.Value('i', RunStatus.about_to_idle)
        run_status_list = [self.detect_status, self.rpe_status, self.main_status] + self.odtw_cores_status
        window_size = (2048 * ref_rate) // 44100
        hop_size = (882 * ref_rate) // 44100
        detect_record_queue = mp.Queue()
        rpe_record_queue = mp.Queue()
        odtw_record_queues = [mp.Queue() for i in range(self.ODTW_CORES)]
        detect_result_queue = mp.Queue()
        rpe_result_queue = mp.Queue()
        odtws_est_time = ArrayOfMPArray(self.ODTW_CORES, self.ODTWS_PER_CORE, 'd')
        odtws_est_tempo = ArrayOfMPArray(self.ODTW_CORES, self.ODTWS_PER_CORE, 'd')
        odtws_est_cost = ArrayOfMPArray(self.ODTW_CORES, self.ODTWS_PER_CORE, 'd')
        odtws_track_duration = ArrayOfMPArray(self.ODTW_CORES, self.ODTWS_PER_CORE, 'd')

        detect_process = mp.Process(target=_music_detection_process,
                                    kwargs={"run_status": self.detect_status,
                                            "record_queue": detect_record_queue,
                                            "result_queue": detect_result_queue,
                                            "now_est_time": self._est_time,
                                            "sample_rate": ref_rate,
                                            "ref_frames_mparr": ref_frames_mparr,
                                            "window_size": window_size,
                                            "hop_size": hop_size,
                                            "FEATURE_DIFF": self.MD_FEATURE_DIFF,
                                            "BUFF_TIME": self.MD_BUFF_TIME})
        rpe_process = mp.Process(target=_rough_pos_estimator_process,
                                 kwargs={"run_status": self.rpe_status,
                                         "record_queue": rpe_record_queue,
                                         "result_queue": rpe_result_queue,
                                         "sample_rate": ref_rate,
                                         "ref_frames_mparr": ref_frames_mparr,
                                         "window_size": window_size,
                                         "hop_size": hop_size,
                                         "FEATURE_DIFF": self.RPE_FEATURE_DIFF,
                                         "BUFF_TIME": self.RPE_BUFF_TIME,
                                         "down_sample": self.RPE_DOWN_SAMPLE,
                                         "ref_resolution": self.RPE_REF_RESOLUTION,
                                         "record_res_ratio": self.RPE_RECORD_RES_RATIO})
        odtw_processes = []
        for i in range(self.ODTW_CORES):
            odtw_processes.append(mp.Process(target=_odtw_core_process,
                                             kwargs={"CORE_ID": i,
                                                     "ODTWS_NUM": self.ODTWS_PER_CORE,
                                                     "core_run_status": self.odtw_cores_status[i],
                                                     "odtws_run_status": self.odtws_status.arrays[i],
                                                     "record_queue": odtw_record_queues[i],
                                                     "est_time": odtws_est_time.arrays[i],
                                                     "est_tempo": odtws_est_tempo.arrays[i],
                                                     "est_cost": odtws_est_cost.arrays[i],
                                                     "track_duration": odtws_track_duration.arrays[i],
                                                     "sample_rate": ref_rate,
                                                     "ref_frames_mparr": ref_frames_mparr,
                                                     "window_size": window_size,
                                                     "hop_size": hop_size,
                                                     "BUFF_TIME": self.ODTW_BUFF_TIME,
                                                     "FEATURE_DIFF": self.ODTW_FEATURE_DIFF}))
        main_process = mp.Process(target=_main_tracking_process,
                                  kwargs={"run_status": self.main_status,
                                          "est_time": self._est_time,
                                          "est_tempo": self._est_tempo,
                                          "record_rate": ref_rate,
                                          "update_freq": self.UPDATE_FREQ,
                                          "odtws_status": self.odtws_status,
                                          "odtws_est_time": odtws_est_time,
                                          "odtws_est_tempo": odtws_est_tempo,
                                          "odtws_est_cost": odtws_est_cost,
                                          "odtws_track_duration": odtws_track_duration,
                                          "detect_record_queue": detect_record_queue,
                                          "rpe_record_queue": rpe_record_queue,
                                          "odtw_record_queues": odtw_record_queues,
                                          "detect_result_queue": detect_result_queue,
                                          "rpe_result_queue": rpe_result_queue,
                                          "command_queue": self.command_queue,
                                          "msg_queue": self.msg_queue})

        self.processes = [detect_process, rpe_process, main_process] + odtw_processes
        detect_process.start()
        rpe_process.start()
        for i in range(self.ODTW_CORES):
            odtw_processes[i].start()
        main_process.start()

        while any(s.value == RunStatus.about_to_idle for s in run_status_list):
            pass
        logging.info("Music tracker is initialized.")

    def end_processes(self):
        self.detect_status.value = RunStatus.terminate
        for i in range(self.ODTW_CORES):
            self.odtw_cores_status[i].value = RunStatus.terminate
        self.rpe_status.value = RunStatus.terminate
        self.main_status.value = RunStatus.terminate
        while any(p.is_alive() for p in self.processes):
            pass

    def change_ref_data(self, ref_filename):
        self.end_processes()
        self.__load_data_and_init_processes(ref_filename)

    def start_tracking(self):
        run_status_list = [self.detect_status, self.rpe_status, self.main_status]
        while any(s == RunStatus.about_to_idle for s in run_status_list):
            pass
        self.detect_status.value = RunStatus.about_to_run
        self.rpe_status.value = RunStatus.about_to_run
        self.main_status.value = RunStatus.about_to_run
        #for i in range(self.ODTW_CORES):
        #    self.odtw_cores_status[i].value = RunStatus.running
        logging.info("Start tracking.")

    def stop_tracking(self):
        #for i in range(self.ODTW_CORES):
        #    self.odtw_cores_status[i].value = RunStatus.idling
        run_status_list = [self.detect_status, self.rpe_status, self.main_status]
        while any(s == RunStatus.about_to_run for s in run_status_list):
            pass
        self.detect_status.value = RunStatus.about_to_idle
        while self.detect_status.value != RunStatus.idling:
            pass
        self.rpe_status.value = RunStatus.about_to_idle
        while self.rpe_status.value != RunStatus.idling:
            pass
        self.main_status.value = RunStatus.about_to_idle
        while self.main_status.value != RunStatus.idling:
            pass
        for i in range(self.odtws_status.size):
            if self.odtws_status[i] != RunStatus.idling:
                while self.odtws_status[i] == RunStatus.about_to_run:
                    pass
                self.odtws_status[i] = RunStatus.about_to_idle
        while any(s == RunStatus.about_to_idle for s in self.odtws_status):
            pass
        # clear msg_queue
        try:
            while not self.msg_queue.empty():
                self.msg_queue.get_nowait()
        except AttributeError:
            pass
        logging.info("Stop tracking.")

    @property
    def est_time(self):
        """
        return -1.0 if not estimating
        """
        return self._est_time.value

    @property
    def est_tempo(self):
        """
        return -1.0 if not estimating
        """
        return self._est_tempo.value

    def is_tracking(self):
        return (self.main_status.value == RunStatus.running)

    def assign_tracking_time(self, new_time):
        if self.ODTW_CORES * self.ODTWS_PER_CORE <= 1:
            raise NotImplementedError
        self.command_queue.put(("FORCE_CHANGE_TIME", new_time))

    def is_paused(self):
        return (self.main_status.value == RunStatus.paused)

    def pause_play_toggle(self):
        if self.main_status.value == RunStatus.running:
            self.detect_status.value = RunStatus.about_to_idle
            while self.detect_status.value != RunStatus.idling:
                pass
            self.rpe_status.value = RunStatus.about_to_idle
            while self.rpe_status.value != RunStatus.idling:
                pass
            self.main_status.value = RunStatus.about_to_paused
            return True
        elif self.main_status.value == RunStatus.paused:
            self.detect_status.value = RunStatus.about_to_run
            self.rpe_status.value = RunStatus.about_to_run
            self.main_status.value = RunStatus.release_from_paused
            return True
        else:
            return False

def get_feature_from_frames(frames, sample_rate, window_size, hop_size, diff):
    f, t, Sxx = spectrogram(frames, window="hamming", fs=sample_rate,
                            nperseg=window_size, noverlap=window_size-hop_size)
    feature = get_chroma_feature_from_spectrogram(Sxx, f, diff)
    t = (t[:-1] + t[1:]) / 2 if diff else t
    return feature, t

def _music_detection_process(*, run_status, record_queue, result_queue, now_est_time,
                             sample_rate, ref_frames_mparr, window_size, hop_size,
                             FEATURE_DIFF, BUFF_TIME):
    logging.info("Music detection process starts.")
    record_queue.cancel_join_thread()
    result_queue.cancel_join_thread()

    ref_frames = np.frombuffer(ref_frames_mparr.get_obj())
    ref_feature, ref_time = get_feature_from_frames(ref_frames, sample_rate, window_size, hop_size, FEATURE_DIFF)
    ref_beginning = ref_feature[:, np.where(ref_time<=0.5)[0]]
    ref_beginning_len = ref_beginning.shape[1]
    buff_frames = np.random.rand(sample_rate * BUFF_TIME)
    total_frames = 0
    next_frame = 0

    logging.info("Music detection process is ready.")
    while True:
        while not record_queue.empty():
            data = record_queue.get_nowait()
            roll_data_to_array(buff_frames, data[:,0])
            total_frames += data.shape[0]

        status_switch = run_status.value
        if status_switch == RunStatus.idling:
            pass
        elif status_switch == RunStatus.running:
            buff_feature, _ = get_feature_from_frames(buff_frames, sample_rate, window_size,
                                                      hop_size, FEATURE_DIFF)
            if now_est_time.value == -1:
                D_begin, wp = simpleDTW(buff_feature[:, -ref_beginning_len:],
                                        ref_beginning, metric="cosine")
                #print(D_begin[-1,-1] / ref_beginning_len)
                if D_begin[-1,-1] / ref_beginning_len < 0.75:
                    result_queue.put((MusicStatus.detected, ref_time[np.argmin(D_begin[-1])]))
        elif status_switch == RunStatus.about_to_idle:
            run_status.value = RunStatus.idling
        elif status_switch == RunStatus.about_to_run:
            run_status.value = RunStatus.running
        elif status_switch == RunStatus.terminate:
            break
        else:
            raise RuntimeError("Undefined behavior in _music_detection_process().")

    logging.info("Music detection process ends.")

def _odtw_core_process(*, CORE_ID, ODTWS_NUM, core_run_status, odtws_run_status, record_queue,
                       est_time, est_tempo, est_cost, track_duration, sample_rate, ref_frames_mparr,
                       window_size, hop_size, FEATURE_DIFF, BUFF_TIME):
    logging.info("ODTW core {} process starts.".format(CORE_ID))
    record_queue.cancel_join_thread()

    ref_frames = np.frombuffer(ref_frames_mparr.get_obj())
    ref_feature, ref_time = get_feature_from_frames(ref_frames, sample_rate, window_size, hop_size, FEATURE_DIFF)
    buff_frames = np.random.rand(sample_rate * BUFF_TIME)
    buff_feature = None
    total_frames = 0
    next_frame = 0
    odtws = [None] * ODTWS_NUM
    running_odtws = 0
    for i in range(ODTWS_NUM):
        est_time[i] = -1
        est_tempo[i] = -1
        track_duration[i] = -1
        est_cost[i] = -1
        odtws_run_status[i] = RunStatus.idling

    core_run_status.value = RunStatus.running
    logging.info("ODTW core {} process is ready.".format(CORE_ID))
    while core_run_status.value != RunStatus.terminate:
        while not record_queue.empty():
            data = record_queue.get_nowait()
            roll_data_to_array(buff_frames, data[:,0])
            total_frames += data.shape[0]
        get_record = False
        if FEATURE_DIFF and total_frames - next_frame >= window_size + hop_size:
            get_record = True
        elif not FEATURE_DIFF and total_frames - next_frame >= window_size:
            get_record = True
        if buff_feature is not None and get_record:
            begin_f = next_frame - total_frames + buff_frames.shape[0]
            added_f = total_frames - next_frame - ((total_frames - next_frame - window_size) % hop_size)
            new_feature, _ = get_feature_from_frames(buff_frames[begin_f:begin_f+added_f],
                                                     sample_rate, window_size, hop_size, FEATURE_DIFF)
            buff_feature.addcols(new_feature)
            if FEATURE_DIFF:
                next_frame = next_frame + added_f - window_size
            else:
                next_frame = next_frame + added_f - window_size + hop_size

        for i in range(ODTWS_NUM):
            status_switch = odtws_run_status[i]
            if status_switch == RunStatus.idling:
                pass
            elif status_switch == RunStatus.running:
                if odtws[i].update():
                    est_time[i] = max(odtws[i].Y_time[odtws[i].y], est_time[i])
                    est_tempo[i] = odtws[i].now_tempo
                    track_duration[i] = odtws[i].t * odtws[i].X_time_diff
                    x_old, y_old = odtws[i].backward_x(100)
                    est_cost[i] = odtws[i].D[odtws[i].x, odtws[i].y] - odtws[i].D[x_old, y_old]
                    est_cost[i] /= ((odtws[i].x - x_old) * odtws[i].X_time_diff)
            elif status_switch == RunStatus.about_to_idle:
                est_time[i] = -1
                est_tempo[i] = -1
                track_duration[i] = -1
                est_cost[i] = -1
                odtws[i] = None
                running_odtws -= 1
                if running_odtws == 0:
                    buff_feature = None
                odtws_run_status[i] = RunStatus.idling
            elif status_switch == RunStatus.about_to_run:
                est_tempo[i] = 1
                if total_frames < window_size + hop_size:
                    pass
                else:
                    if buff_feature is None:
                        next_frame = total_frames - window_size - hop_size
                        buff_feature = Expandable2DArray((ref_feature.shape[0],0))
                    elif buff_feature.shape[1] > 0:
                        x_start = buff_feature.shape[1] - 1
                        start_frame = (np.abs(ref_time - est_time[i])).argmin()
                        odtws[i] = ODTW(buff_feature, ref_feature[:, start_frame:],
                                        c=250, X_start=x_start, use_tempo_model=True,
                                        Y_time=ref_time[start_frame:])
                        running_odtws += 1
                        odtws_run_status[i] = RunStatus.running
            elif status_switch == RunStatus.paused:
                pass
            else:
                error_msg = "Undefined behavior in _odtw_core_process(c: {}, i: {}).".format(CORE_ID, i)
                raise RuntimeError(error_msg)

    logging.info("ODTW core {} process ends.".format(CORE_ID))

def _rough_pos_estimator_process(*, run_status, record_queue, result_queue, sample_rate,
                                 ref_frames_mparr, window_size, hop_size, FEATURE_DIFF, BUFF_TIME,
                                 down_sample, ref_resolution, record_res_ratio):
    logging.info("RPE process starts.")
    record_queue.cancel_join_thread()
    result_queue.cancel_join_thread()

    ref_frames = np.frombuffer(ref_frames_mparr.get_obj())
    ref_feature, ref_time = get_feature_from_frames(ref_frames, sample_rate, window_size, hop_size, FEATURE_DIFF)
    hann_filter = np.hanning(down_sample)
    ref_rough_feature = convolve1d(ref_feature, hann_filter, mode="same")[:, ::ref_resolution]
    ref_rough_time = ref_time[::ref_resolution]

    buff_frames = np.random.rand(sample_rate * BUFF_TIME)
    buff_feature, _ = get_feature_from_frames(buff_frames, sample_rate, window_size, hop_size, FEATURE_DIFF)
    total_frames = 0
    next_frame = 0
    dtw_steps = np.array([[2, 1], [1, 2], [1, 1]])
    dtw_steps[:, 1] *= record_res_ratio
    dtw_step_weight = np.array([2.0, 1.0, 1.0])

    logging.info("RPE process is ready.")
    while True:
        while not record_queue.empty():
            data = record_queue.get_nowait()
            roll_data_to_array(buff_frames, data[:,0])
            total_frames += data.shape[0]
        get_record = False
        if FEATURE_DIFF and total_frames - next_frame >= window_size + hop_size:
            get_record = True
        elif not FEATURE_DIFF and total_frames - next_frame >= window_size:
            get_record = True
        if get_record:
            begin_f = next_frame - total_frames + buff_frames.shape[0]
            added_f = total_frames - next_frame - ((total_frames - next_frame - window_size) % hop_size)
            new_feature, _ = get_feature_from_frames(buff_frames[begin_f:begin_f+added_f],
                                                     sample_rate, window_size, hop_size, FEATURE_DIFF)
            roll_2Ddata_to_array_by_row(buff_feature, new_feature)
            if FEATURE_DIFF:
                next_frame = next_frame + added_f - window_size
            else:
                next_frame = next_frame + added_f - window_size + hop_size

        status_switch = run_status.value
        if status_switch == RunStatus.idling:
            pass
        elif status_switch == RunStatus.running:
            if get_record:
                buff_rough_feature = convolve1d(buff_feature, hann_filter, mode="same")
                buff_rough_feature = buff_rough_feature[:, -2::-ref_resolution*record_res_ratio][:, ::-1]
                D, wp = simpleGreedyBackwardTW(buff_rough_feature, ref_rough_feature,
                                               metric="cosine", STEPS=dtw_steps, STEP_WEIGHT=dtw_step_weight)
                possible = np.where( D[-1, :] < np.percentile(D[-1, :], 5))[0]
                possible = np.split(possible, np.where(np.diff(possible) > 2)[0]+1)
                possible = [p[np.argmin(D[-1, p])] for p in possible]
                possible = [ref_rough_time[possible[p]] for p in np.argsort(D[-1, possible])]

                result_queue.put(np.array(possible))
        elif status_switch == RunStatus.about_to_idle:
            run_status.value = RunStatus.idling
        elif status_switch == RunStatus.about_to_run:
            run_status.value = RunStatus.running
        elif status_switch == RunStatus.terminate:
            break
        else:
            raise RuntimeError("Undefined behavior in _rough_pos_estimator_process().")

    logging.info("RPE process ends.")

def _main_tracking_process(*, run_status, est_time, est_tempo, record_rate, update_freq,
                           odtws_status, odtws_est_time, odtws_est_tempo, odtws_est_cost,
                           odtws_track_duration, detect_record_queue, rpe_record_queue,
                           odtw_record_queues, detect_result_queue, rpe_result_queue,
                           command_queue, msg_queue):
    logging.info("Music tracking process starts.")
    detect_record_queue.cancel_join_thread()
    detect_result_queue.cancel_join_thread()
    rpe_record_queue.cancel_join_thread()
    rpe_result_queue.cancel_join_thread()
    for orq in odtw_record_queues:
        orq.cancel_join_thread()
    msg_queue.cancel_join_thread()

    record_stream = RecordStream(record_rate, 1)
    record_stream.start()
    refresh_period = 1 / update_freq
    now_time = time.time()
    now_time_tick = now_time
    tick = False
    trusted_odtw = -1
    odtws_cost_sum = [0] * odtws_status.size
    logging.info("Music tracking process is ready.")
    while True:
        while not record_stream.data_queue.empty():
            data = record_stream.data_queue.get_nowait()
            if run_status.value != RunStatus.paused:
                detect_record_queue.put(data)
                rpe_record_queue.put(data)
                for orq in odtw_record_queues:
                    orq.put(data)
        tick = False
        now_time = time.time()
        if now_time - now_time_tick > refresh_period:
            now_time_tick = now_time - (now_time - now_time_tick) % refresh_period
            tick = True

        status_switch = run_status.value
        if status_switch == RunStatus.idling:
            pass
        elif status_switch == RunStatus.running:
            if trusted_odtw != -1 and tick:
                if odtws_track_duration[trusted_odtw] > 1:
                    trusted_cost = odtws_est_cost[trusted_odtw]
                    running_odtws = [i for i in range(odtws_status.size)
                                     if i != trusted_odtw and odtws_status[i] == RunStatus.running
                                     and odtws_track_duration[i] > 1]
                    for r in running_odtws:
                        odtws_cost_sum[r] += trusted_cost - odtws_est_cost[r]
                    trusted_next = -1
                    for r in running_odtws:
                        r_dt = odtws_est_time[r] - odtws_est_time[trusted_odtw]
                        if r_dt >= 0 and r_dt < 0.05 and odtws_track_duration[r] > 10:
                            trusted_next = r
                            break
                        elif odtws_cost_sum[r] > 15 * trusted_cost:
                            if trusted_next == -1:
                                trusted_next = r
                            else:
                                trusted_next = r if odtws_cost_sum[r] > odtws_cost_sum[trusted_next] \
                                               else trusted_next
                        elif abs(r_dt) > 9 \
                             or odtws_cost_sum[r] < -2 * trusted_cost:
                            odtws_cost_sum[r] = 0
                            odtws_status[r] = RunStatus.about_to_idle
                    if trusted_next != -1:
                        #odtws_status[trusted_odtw] = RunStatus.about_to_idle
                        odtws_cost_sum = [0] * odtws_status.size
                        trusted_odtw = trusted_next
                est_time.value = odtws_est_time[trusted_odtw]
                est_tempo.value = odtws_est_tempo[trusted_odtw]
                odtws_timelist = ["odtws_timelist", trusted_odtw]
                odtws_timelist.extend([odtws_est_time[i] for i in range(odtws_status.size)])
                msg_queue.put(tuple(odtws_timelist))
            detect_result = None
            while not detect_result_queue.empty():
                detect_result = detect_result_queue.get_nowait()
            if detect_result is not None:
                if detect_result[0] == MusicStatus.detected:
                    if trusted_odtw == -1:
                        for i in range(odtws_status.size):
                            if odtws_status[i] == RunStatus.idling:
                                trusted_odtw = i
                                break
                        est_time.value = detect_result[1]
                        odtws_est_time[trusted_odtw] = detect_result[1]
                        odtws_status[trusted_odtw] = RunStatus.about_to_run
                    msg_queue.put(("DETECT", detect_result[1]))
            rpe_result = None
            while not rpe_result_queue.empty():
                rpe_result = rpe_result_queue.get_nowait()
            commands = {}
            while not command_queue.empty():
                c = command_queue.get_nowait()
                commands[c[0]] = c[1:]
            if "FORCE_CHANGE_TIME" in commands:
                idle_odtw = -1
                for i in range(odtws_status.size):
                    if odtws_status[i] == RunStatus.idling:
                        idle_odtw = i
                        break
                    elif i == odtws_status.size - 1:
                        idle_odtw = i - 1 if i == trusted_odtw else i
                        odtws_cost_sum[idle_odtw] = 0
                        while odtws_status[idle_odtw] == RunStatus.about_to_run:
                            pass
                        if odtws_status[idle_odtw] == RunStatus.running:
                            odtws_status[idle_odtw] = RunStatus.about_to_idle
                        while odtws_status[idle_odtw] != RunStatus.idling:
                            pass
                odtws_est_time[idle_odtw] = commands["FORCE_CHANGE_TIME"][0]
                odtws_status[idle_odtw] = RunStatus.about_to_run
                trusted_odtw = idle_odtw
            else:
                if rpe_result is not None:
                    if est_time.value != -1:
                        msg_queue.put(("RPE", rpe_result[np.argmin(np.abs(rpe_result - est_time.value))]))
                    else:
                        msg_queue.put(("RPE", rpe_result.min()))
                    #rpe_result = rpe_result[np.argsort(np.abs(rpe_result - est_time.value))]
                    if est_time.value != -1:
                        for p in range(min(3, len(rpe_result))):
                            if abs(rpe_result[p] - est_time.value) < 9:
                                idle_odtw = -1
                                for i in range(odtws_status.size):
                                    if odtws_status[i] == RunStatus.idling and idle_odtw == -1:
                                        idle_odtw = i
                                    elif abs(rpe_result[p] - odtws_est_time[i]) < 0.5:
                                        idle_odtw = -1
                                        break
                                if idle_odtw != -1:
                                    odtws_est_time[idle_odtw] = rpe_result[p]
                                    odtws_status[idle_odtw] = RunStatus.about_to_run
            if trusted_odtw != -1:
                est_time.value = odtws_est_time[trusted_odtw]
                est_tempo.value = odtws_est_tempo[trusted_odtw]
        elif status_switch == RunStatus.about_to_idle:
            est_time.value = -1
            est_tempo.value = -1
            # clear queues
            while not rpe_result_queue.empty():
                rpe_result_queue.get_nowait()
            while not detect_result_queue.empty():
                detect_result_queue.get_nowait()
            while not command_queue.empty():
                command_queue.get_nowait()
            run_status.value = RunStatus.idling
        elif status_switch == RunStatus.about_to_run:
            trusted_odtw = -1
            run_status.value = RunStatus.running
        elif status_switch == RunStatus.terminate:
            break
        elif status_switch == RunStatus.about_to_paused:
            for i in range(odtws_status.size):
                while odtws_status[i] == RunStatus.about_to_run:
                    pass
                if odtws_status[i] == RunStatus.running:
                    odtws_status[i] = RunStatus.paused
            # clear queues
            while not rpe_result_queue.empty():
                rpe_result_queue.get_nowait()
            while not detect_result_queue.empty():
                detect_result_queue.get_nowait()
            while not command_queue.empty():
                command_queue.get_nowait()
            est_tempo.value = 0
            run_status.value = RunStatus.paused
        elif status_switch == RunStatus.paused:
            pass
        elif status_switch == RunStatus.release_from_paused:
            for i in range(odtws_status.size):
                if odtws_status[i] == RunStatus.paused:
                    odtws_status[i] = RunStatus.running
            est_tempo.value = odtws_est_tempo[trusted_odtw]
            run_status.value = RunStatus.running
        else:
            raise RuntimeError("Undefined behavior in _main_tracking_process().")

    logging.info("Music tracking process ends.")

if __name__ == "__main__":
    import sys
    tracker = MusicTracker(ODTW_CORES=2, ODTWS_PER_CORE=2, ref_filename=sys.argv[1])
    tracker.start_tracking()
    time.sleep(10)
    tracker.stop_tracking()
    time.sleep(5)
    tracker.start_tracking()
    time.sleep(1)
    tracker.stop_tracking()
    time.sleep(1)
    tracker.end_processes()
