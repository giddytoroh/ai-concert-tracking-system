import queue
import numpy as np
import sounddevice
import soundfile
from datetime import datetime

class RecordStream:
    """
    a wrapper of sounddevice.InputStream with a queue.Queue to get data
    """

    def __init__(self, rate, channels, device=None):
        self.device = device if device is not None else sounddevice.default.device[0]
        self.sampling_channels = channels
        self.sampling_rate = rate
        self.data_queue = queue.Queue()

        def input_callback(q):
            def f(indata, frames, time, status):
                q.put(np.copy(indata))
            return f

        self.input_stream = sounddevice.InputStream(channels=self.sampling_channels,
                                                    samplerate=self.sampling_rate,
                                                    callback=input_callback(self.data_queue),
                                                    device=self.device)
    
    def start(self):
        self.input_stream.start()

    def stop(self, abort=False):
        if abort:
            self.input_stream.abort()
        else:
            self.input_stream.stop()

    @property
    def stopped(self):
        return self.input_stream.stopped

    def close(self):
        if not self.input_stream.closed:
            self.input_stream.close()

    @property
    def closed(self):
        return self.input_stream.closed

    def reset(self):
        if not self.input_stream.stopped:
            self.input_stream.abort()
        with self.data_queue.mutex:
            self.data_queue.queue.clear()

    def save_file(self, **kwargs):
        data = list(self.data_queue.queue)
        if len(data) > 0:
            data= np.concatenate(data)
            fileformat = kwargs["format"] if "format" in kwargs else ".wav"
            filename = kwargs["filename"] if "filename" in kwargs \
                    else datetime.now().strftime("%Y_%m_%d_%H_%M_%S") + fileformat
            soundfile.write(filename, data, self.sampling_rate)

        reset = kwargs["reset"] if "reset" in kwargs else False
        if reset:
            self.reset()
    
    def all_data(self):
        return np.concatenate(list(self.data_queue.queue))


if __name__ == "__main__":
    import time
    r = RecordStream(22050, 1)
    r.start()
    time.sleep(20)
    r.stop()
    r.save_file()
    r.close()