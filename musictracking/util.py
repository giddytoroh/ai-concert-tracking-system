import numpy as np
from numpy.linalg import norm

def roll_data_to_array(arr, data):
    shift = len(data)
    arr[:-shift] = arr[shift:]
    arr[-shift:] = data

def roll_2Ddata_to_array_by_row(arr, data):
    shift = data.shape[1]
    arr[:, :-shift] = arr[:, shift:]
    arr[:, -shift:] = data

convolve1d = np.vectorize(np.convolve, signature="(n),(m)->(k)", excluded=["mode"])

def get_chroma_feature_from_spectrogram(spectrogram, freq, diff=False):
    freq_threshold = 440/(2**(3/12))
    idx = (np.abs(freq - freq_threshold)).argmin() # F#4
    bins = [spectrogram[i].reshape(1,-1) for i in range(idx+1)]

    freq_start = 440/(2**(2.5/12)) # lower than G4
    for i in range(61):
        idx = np.where(np.logical_and(freq>=freq_start, freq<freq_start*(2**(1/12))))
        if len(idx[0]) == 0:
            break
        bins.append(np.sum(spectrogram[idx], axis=0).reshape(1,-1))
        freq_start *= 2**(1/12)
    #idx = np.where(freq>=freq_start)
    #bins.append(np.sum(spectrogram[idx], axis=0).reshape(1,-1))
    bins = np.concatenate(bins, axis=0)
    if diff:
        bins = np.maximum(np.diff(bins, axis=1), 1e-20)
    bsum = np.sum(bins, axis=0)
    bins = bins/bsum

    return bins

class Expandable1DArray:
    def __init__(self, init):
        if isinstance(init, int):
            if init < 0:
                raise ValueError
            self.length = init
            self.capacity = max(8, init * 2)
            self.data = np.zeros(self.capacity)
            self.array = self.data[:self.length]
        else:
            try:
                init = np.asarray(init)
                assert init.ndim == 1
            except:
                raise ValueError
            self.length = init.shape[0]
            self.capacity = max(8, self.length * 2)
            self.data = np.zeros(self.capacity)
            self.data[:self.length] = init
            self.array = self.data[:self.length]

    def extend(self, d):
        d = np.asarray(d)
        assert d.ndim == 1

        new_n = self.length + d.shape[0]
        if new_n >= self.capacity:
            self.capacity = self.capacity * (1 + new_n // self.capacity)
            newdata = np.zeros(self.capacity)
            newdata[:self.length] = self.data[:self.length]
            self.data = newdata

        self.data[self.length:new_n] = d
        self.length = new_n
        self.array = self.data[:self.length]

    def __getitem__(self, key):
        return self.array[key]

    def __setitem__(self, idx, value):
        self.array[idx] = value

    def __len__(self):
        return self.length

class Expandable2DArray:
    def __init__(self, init):
        if isinstance(init, tuple) or isinstance(init, list):
            if len(init) != 2:
                raise ValueError
            self.shape = (init[0], init[1])
            self.capacity = (max(4, init[0]), max(4, init[1]))
            self.data = np.zeros(self.capacity)
            self.array = self.data[:self.shape[0], :self.shape[1]]
        elif isinstance(init, np.ndarray):
            init = np.atleast_2d(init)
            if init.ndim > 2:
                raise TypeError
            self.shape = init.shape
            self.capacity = (max(4, self.shape[0]), max(4, self.shape[1]))
            self.data = np.zeros(self.capacity)
            self.data[:self.shape[0], :self.shape[1]] = init
            self.array = self.data[:self.shape[0], :self.shape[1]]
        else:
            raise TypeError

    def addrows(self, rows):
        if isinstance(rows, list):
            rows = [r.reshape(1,-1) for r in rows]
            rows = np.concatenate(rows, axis=0)
        elif rows.ndim == 1:
            rows = rows.reshape(1,-1)
        if rows.ndim > 2 or rows.shape[1] != self.shape[1]:
            raise ValueError("Wrong row dimension!")

        new_rows = self.shape[0] + rows.shape[0]
        if new_rows >= self.capacity[0]:
            new_row_capacity = self.capacity[0] * (1 + new_rows // self.capacity[0])
            self.capacity = (new_row_capacity, self.capacity[1])
            newdata = np.zeros(self.capacity)
            newdata[:self.shape[0], :] = self.data[:self.shape[0], :]
            self.data = newdata

        self.data[self.shape[0]:new_rows, :self.shape[1]] = rows
        self.shape = (new_rows, self.shape[1])
        self.array = self.data[:self.shape[0], :self.shape[1]]

    def addcols(self, cols):
        if isinstance(cols, list):
            cols = [c.reshape(-1,1) for c in cols]
            cols = np.concatenate(cols, axis=1)
        elif cols.ndim == 1:
            cols = cols.reshape(-1,1)
        if cols.ndim > 2 or cols.shape[0] != self.shape[0]:
            raise ValueError("Wrong column dimension!")

        new_cols = self.shape[1] + cols.shape[1]
        if new_cols >= self.capacity[1]:
            new_col_capacity = self.capacity[1] * (1 + new_cols // self.capacity[1])
            self.capacity = (self.capacity[0], new_col_capacity)
            newdata = np.zeros(self.capacity)
            newdata[:, :self.shape[1]] = self.data[:, :self.shape[1]]
            self.data = newdata

        self.data[:self.shape[0], self.shape[1]:new_cols] = cols
        self.shape = (self.shape[0], new_cols)
        self.array = self.data[:self.shape[0], :self.shape[1]]

    def __getitem__(self, idx):
        return self.array[idx]

    def __setitem__(self, idx, value):
        self.array[idx] = value


if __name__ == "__main__":
    pass